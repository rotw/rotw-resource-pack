- "Spyro's Wings" by [TCK436](http://www.minecraftforum.net/forums/mapping-and-modding-java-edition/resource-packs/2541424-spyro-the-dragons-elytra-wings-1-9)
- "Pixel Wings" from [Pixel Perfection by XSSheep](http://www.minecraftforum.net/forums/mapping-and-modding-java-edition/resource-packs/1242533-pixel-perfection-now-with-polar-bears-1-11)
- Multiple elytras from [Additional Eytras & Invisible Armor by Ontvlambaar](https://www.planetminecraft.com/texture_pack/more-elytras-in-vanilla-minecraft-requires-optifine/)
    - Multiple elytras by [Five (Paradiscal)](https://www.planetminecraft.com/texture_pack/paradiscals-elytra-mcpe/)  
      *Originally made for Pocket Edition*