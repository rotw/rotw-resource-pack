const
    gulp = require('gulp-help')(require('gulp')),
    gutil = require('gulp-util'),
    fs = require('fs'),
    path = require('path'),
    argv = require('yargs').argv,
    bump = require('gulp-bump'),
    glob = require("glob"),
    jsonfile = require('jsonfile'),
    AdmZip = require('adm-zip'),
    multimatch = require('multimatch');

function getBumpType() {
    if (argv.major !== undefined) return 'major';
    else if (argv.minor !== undefined) return 'minor';
    else if (argv.prerelease !== undefined || argv.pre !== undefined) return 'prerelease';
    else if (argv.patch !== undefined) return 'patch';
    else return null;
}

gulp.task('default', ['bump', 'zip'], function() {
    gutil.log('Build complete.');
});

gulp.task('bump', 'Increments the version.', function() {
    var bumpType = getBumpType();

    if (bumpType) {
        return gulp.src('./package.json')
            .pipe(bump({ type: bumpType }))
            .pipe(gulp.dest('./'));
    } else {
        gutil.log("No bump type specified; Skip.")
    }
});

gulp.task('zip', 'Zips the resource pack and removes blacklisted files.', ['bump'], function() {
    var package = jsonfile.readFileSync('package.json');
    var config = jsonfile.readFileSync('config.json');
    var zip = new AdmZip();

    zip.addLocalFolder('src');

    var mcmeta = {
        "pack": {
            "pack_format": config.pack_format,
            "description": package.description +
                "\nAuthor: " + package.author +
                "\nCredits: " + package.contributors.join(', ')
        }
    };

    zip.addFile("pack.mcmeta", new Buffer(JSON.stringify(mcmeta)));

    glob.sync('*.md').forEach(file => zip.addLocalFile(file));

    // Delete zipped files matching the glob patterns in config.zip.exclude
    multimatch(zip.getEntries().map(e => e.entryName), config.zip.exclude).forEach(e => zip.deleteFile(e));

    var distpath = 'dist/' + config.zip.prefix + package.version + '.zip';

    if (fs.existsSync(distpath)) {
        gutil.log('A distributable pack already exists for this version. Did you forget to use --patch?');
    } else {
        zip.writeZip(distpath);
        gutil.log('Created distributable pack at: ' + distpath);
    }

    zip.writeZip(config.zip.devpath);
    gutil.log('Created development pack at: ' + config.zip.devpath);
});

gulp.task('js', 'Runs all *.js modules in src/.', function() {
    glob("./src/**/*.js", function(err, files) {
        if (err) throw err;
        var startDir = process.cwd();

        files.forEach(file => {
            gutil.log('Running script: ' + file);
            var mod = require(file);

            process.chdir(path.dirname(file));
            mod();
            process.chdir(startDir);
        });
    });
});